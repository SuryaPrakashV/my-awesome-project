describe('my first test', function() {
	it('login', function() {
		cy.visit('app1.html')
		cy.get('input[name=firstname]').focus().type('Sathya')
		  .should('have.value', 'Sathya')
		  .wait(500)
		cy.get('input[name=lastname]').focus().type('Murugesan')
		  .should('have.value', 'Murugesan')
		  .wait(500)
	})
	it('selection',function() {
 		cy.get('select')
		  .select('Volvo').should('have.value','volvo')
		  .select('Fiat').should('have.value','fiat')
		  .select('Saab').should('have.value','saab')
		  .select('Audi').should('have.value','audi')
	})
	it('button test', function() {
		const stub = cy.stub() 
		cy.on('window:alert',stub)
		cy.get('button')
		  .contains('Click Me!')
		  .click()
		  .then(() => {
           expect(stub.getCall(0)).to.be.calledWith('Hello World!')  
		  })
		cy.get('button')
		  .contains('Click here!')
		  .click()
		  .then(() => {
           expect(stub.getCall(0)).to.be.calledWith('Hello World!')  
		  })
	})
	it('list test', function() {
		cy.get('form').submit()
		cy.get('li')
		  .eq(0)
		  .should('contain','Coffee')
		cy.get('li')
    	  .eq(1)
		  .should('contain','Tea')
		cy.get('li').should('have.length',3);
		cy.get('ul').should('have.css','color','rgb(255, 0, 0)')

	})
    it('checkbox', function() {
		cy.get('input[type=checkbox]').check()
		  .eq(1).uncheck()
		cy.get('.add').eq(0).check()
	})
	it('requesting', function() {
		cy.visit('final.html')
	    cy.window().its("performance").invoke('getEntriesByType','resource').as('win')
		.each(($el,index,$list) =>
		{
			cy.log($el.name+" "+$el.initiatorType)
		})
 

			 
		
	})
})